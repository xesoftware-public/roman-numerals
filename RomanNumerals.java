public class RomanNumerals {

    private static final String[] ONES =      {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
    private static final String[] TENS =      {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    private static final String[] HUNDREDS =  {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
    private static final String[] THOUSANDS = {"", "M", "MM", "MMM"};

    private static final String[][] DECIMAL = {THOUSANDS, HUNDREDS, TENS, ONES};
    private static final int[] FACTOR = {1000, 100, 10, 1};

    private static String convert(final int value) {
        if (value < 0 || value > 3999) throw new RuntimeException("Out of range");

        return THOUSANDS[(value / 1000) % 10] +
                HUNDREDS[(value / 100) % 10] +
                TENS[(value / 10) % 10] +
                ONES[value % 10];
    }

    private static int convert(final String value) {
        if (value == null || value.isEmpty()) return 0;
        if (!value.matches("[IVXLCDM]*")) return 0;
        return recursivelyConvertRomanNumeralsToDecimal(value, 0, 0);
    }

    private static int recursivelyConvertRomanNumeralsToDecimal(final String value, int index, int currentValue) {
        if (index < 0 || index >= DECIMAL.length) return currentValue;
        final int factor = FACTOR[index];
        final String[] romanNumerals = DECIMAL[index];
        for (int i = romanNumerals.length - 1; i > 0; i--) {
            if (value.startsWith(romanNumerals[i])) {
                return recursivelyConvertRomanNumeralsToDecimal(value.substring(romanNumerals[i].length()), index+1, currentValue + (factor * i));
            }
        }
        return recursivelyConvertRomanNumeralsToDecimal(value, index+1, currentValue);
    }

    public static void main(final String[] args) {
        if (args.length == 0) {
            System.err.println("Usage: java RomanNumerals.java <roman numeral | decimal number>");
            System.exit(-1);
        }

        System.out.println(convertValue(args[0]));
    }

    public static String convertValue(final String value) {
        try {
            int decimalNumber = Integer.parseUnsignedInt(value);
            return convert(decimalNumber);
        } catch (NumberFormatException e) {
            return ""+convert(value);
        }
    }
}
