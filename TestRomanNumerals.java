public class TestRomanNumerals {

    static int STATUS = 0;

    public static void main(final String[] args) {
        check("123", "CXXIII");

        check("0", "");
        check("1", "I");
        check("2", "II");
        check("3", "III");
        check("4", "IV");
        check("5", "V");
        check("6", "VI");
        check("7", "VII");
        check("8", "VIII");
        check("9", "IX");
        check("10", "X");
        check("11", "XI");

        check("505", "DV");

        check("111", "CXI");
        check("222", "CCXXII");
        check("333", "CCCXXXIII");
        check("444", "CDXLIV");
        check("555", "DLV");
        check("666", "DCLXVI");
        check("777", "DCCLXXVII");
        check("888", "DCCCLXXXVIII");
        check("999", "CMXCIX");

        check("505", "DV");


        check("0", "");
        check("3999", "MMMCMXCIX");

        System.exit(STATUS);
    }

    public static void check(String value, String expected) {
        final String result = RomanNumerals.convertValue(value);
        if (!expected.equals(result)) {
            System.err.println("Failed to convert " + value + " to " + expected + " instead got " + result);
            STATUS = -1;
        } else {

            final String value2 = RomanNumerals.convertValue(result);
            if (!value2.equals(value)) {
                System.err.println("Failed to convert " + result + " to " + value + " instead got " + value2);
                STATUS = -1;
            }
            System.out.println("Successfully converted " + value + " to " + expected + " and back again");
        }
    }
}
