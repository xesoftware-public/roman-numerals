# roman-numerals

Stand alone java script to convert to and from decimal and roman numerals.

```
Usage: java RomanNumerals.java <roman numeral | decimal number>
```

E.g.
```
java RomanNumerals.java IV
4
java RomanNumerals.java 4
IV
```

Works up to values of 3999

Tests are in TestRomanNumerals.